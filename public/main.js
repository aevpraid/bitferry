/******/ (function(modules) { // webpackBootstrap
/******/ 	function hotDisposeChunk(chunkId) {
/******/ 		delete installedChunks[chunkId];
/******/ 	}
/******/ 	var parentHotUpdateCallback = window["webpackHotUpdate"];
/******/ 	window["webpackHotUpdate"] = 
/******/ 	function webpackHotUpdateCallback(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		hotAddUpdateChunk(chunkId, moreModules);
/******/ 		if(parentHotUpdateCallback) parentHotUpdateCallback(chunkId, moreModules);
/******/ 	} ;
/******/ 	
/******/ 	function hotDownloadUpdateChunk(chunkId) { // eslint-disable-line no-unused-vars
/******/ 		var head = document.getElementsByTagName("head")[0];
/******/ 		var script = document.createElement("script");
/******/ 		script.type = "text/javascript";
/******/ 		script.charset = "utf-8";
/******/ 		script.src = __webpack_require__.p + "" + chunkId + "." + hotCurrentHash + ".hot-update.js";
/******/ 		;
/******/ 		head.appendChild(script);
/******/ 	}
/******/ 	
/******/ 	function hotDownloadManifest(requestTimeout) { // eslint-disable-line no-unused-vars
/******/ 		requestTimeout = requestTimeout || 10000;
/******/ 		return new Promise(function(resolve, reject) {
/******/ 			if(typeof XMLHttpRequest === "undefined")
/******/ 				return reject(new Error("No browser support"));
/******/ 			try {
/******/ 				var request = new XMLHttpRequest();
/******/ 				var requestPath = __webpack_require__.p + "" + hotCurrentHash + ".hot-update.json";
/******/ 				request.open("GET", requestPath, true);
/******/ 				request.timeout = requestTimeout;
/******/ 				request.send(null);
/******/ 			} catch(err) {
/******/ 				return reject(err);
/******/ 			}
/******/ 			request.onreadystatechange = function() {
/******/ 				if(request.readyState !== 4) return;
/******/ 				if(request.status === 0) {
/******/ 					// timeout
/******/ 					reject(new Error("Manifest request to " + requestPath + " timed out."));
/******/ 				} else if(request.status === 404) {
/******/ 					// no update available
/******/ 					resolve();
/******/ 				} else if(request.status !== 200 && request.status !== 304) {
/******/ 					// other failure
/******/ 					reject(new Error("Manifest request to " + requestPath + " failed."));
/******/ 				} else {
/******/ 					// success
/******/ 					try {
/******/ 						var update = JSON.parse(request.responseText);
/******/ 					} catch(e) {
/******/ 						reject(e);
/******/ 						return;
/******/ 					}
/******/ 					resolve(update);
/******/ 				}
/******/ 			};
/******/ 		});
/******/ 	}
/******/
/******/ 	
/******/ 	
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "5d08f5bc6e8817452058"; // eslint-disable-line no-unused-vars
/******/ 	var hotRequestTimeout = 10000;
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentChildModule; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParentsTemp = []; // eslint-disable-line no-unused-vars
/******/ 	
/******/ 	function hotCreateRequire(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var me = installedModules[moduleId];
/******/ 		if(!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if(me.hot.active) {
/******/ 				if(installedModules[request]) {
/******/ 					if(installedModules[request].parents.indexOf(moduleId) < 0)
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 				} else {
/******/ 					hotCurrentParents = [moduleId];
/******/ 					hotCurrentChildModule = request;
/******/ 				}
/******/ 				if(me.children.indexOf(request) < 0)
/******/ 					me.children.push(request);
/******/ 			} else {
/******/ 				console.warn("[HMR] unexpected require(" + request + ") from disposed module " + moduleId);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		var ObjectFactory = function ObjectFactory(name) {
/******/ 			return {
/******/ 				configurable: true,
/******/ 				enumerable: true,
/******/ 				get: function() {
/******/ 					return __webpack_require__[name];
/******/ 				},
/******/ 				set: function(value) {
/******/ 					__webpack_require__[name] = value;
/******/ 				}
/******/ 			};
/******/ 		};
/******/ 		for(var name in __webpack_require__) {
/******/ 			if(Object.prototype.hasOwnProperty.call(__webpack_require__, name) && name !== "e") {
/******/ 				Object.defineProperty(fn, name, ObjectFactory(name));
/******/ 			}
/******/ 		}
/******/ 		fn.e = function(chunkId) {
/******/ 			if(hotStatus === "ready")
/******/ 				hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			return __webpack_require__.e(chunkId).then(finishChunkLoading, function(err) {
/******/ 				finishChunkLoading();
/******/ 				throw err;
/******/ 			});
/******/ 	
/******/ 			function finishChunkLoading() {
/******/ 				hotChunksLoading--;
/******/ 				if(hotStatus === "prepare") {
/******/ 					if(!hotWaitingFilesMap[chunkId]) {
/******/ 						hotEnsureUpdateChunk(chunkId);
/******/ 					}
/******/ 					if(hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 						hotUpdateDownloaded();
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 		return fn;
/******/ 	}
/******/ 	
/******/ 	function hotCreateModule(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 			_main: hotCurrentChildModule !== moduleId,
/******/ 	
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfAccepted = true;
/******/ 				else if(typeof dep === "function")
/******/ 					hot._selfAccepted = dep;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback || function() {};
/******/ 				else
/******/ 					hot._acceptedDependencies[dep] = callback || function() {};
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfDeclined = true;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 				else
/******/ 					hot._declinedDependencies[dep] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if(idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if(!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if(idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		hotCurrentChildModule = undefined;
/******/ 		return hot;
/******/ 	}
/******/ 	
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/ 	
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for(var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/ 	
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailableFilesMap = {};
/******/ 	var hotDeferred;
/******/ 	
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/ 	
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = (+id) + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/ 	
/******/ 	function hotCheck(apply) {
/******/ 		if(hotStatus !== "idle") throw new Error("check() is only allowed in idle status");
/******/ 		hotApplyOnUpdate = apply;
/******/ 		hotSetStatus("check");
/******/ 		return hotDownloadManifest(hotRequestTimeout).then(function(update) {
/******/ 			if(!update) {
/******/ 				hotSetStatus("idle");
/******/ 				return null;
/******/ 			}
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			hotAvailableFilesMap = update.c;
/******/ 			hotUpdateNewHash = update.h;
/******/ 	
/******/ 			hotSetStatus("prepare");
/******/ 			var promise = new Promise(function(resolve, reject) {
/******/ 				hotDeferred = {
/******/ 					resolve: resolve,
/******/ 					reject: reject
/******/ 				};
/******/ 			});
/******/ 			hotUpdate = {};
/******/ 			var chunkId = 0;
/******/ 			{ // eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if(hotStatus === "prepare" && hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 			return promise;
/******/ 		});
/******/ 	}
/******/ 	
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		if(!hotAvailableFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for(var moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if(!hotAvailableFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var deferred = hotDeferred;
/******/ 		hotDeferred = null;
/******/ 		if(!deferred) return;
/******/ 		if(hotApplyOnUpdate) {
/******/ 			// Wrap deferred object in Promise to mark it as a well-handled Promise to
/******/ 			// avoid triggering uncaught exception warning in Chrome.
/******/ 			// See https://bugs.chromium.org/p/chromium/issues/detail?id=465666
/******/ 			Promise.resolve().then(function() {
/******/ 				return hotApply(hotApplyOnUpdate);
/******/ 			}).then(
/******/ 				function(result) {
/******/ 					deferred.resolve(result);
/******/ 				},
/******/ 				function(err) {
/******/ 					deferred.reject(err);
/******/ 				}
/******/ 			);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for(var id in hotUpdate) {
/******/ 				if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			deferred.resolve(outdatedModules);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotApply(options) {
/******/ 		if(hotStatus !== "ready") throw new Error("apply() is only allowed in ready status");
/******/ 		options = options || {};
/******/ 	
/******/ 		var cb;
/******/ 		var i;
/******/ 		var j;
/******/ 		var module;
/******/ 		var moduleId;
/******/ 	
/******/ 		function getAffectedStuff(updateModuleId) {
/******/ 			var outdatedModules = [updateModuleId];
/******/ 			var outdatedDependencies = {};
/******/ 	
/******/ 			var queue = outdatedModules.slice().map(function(id) {
/******/ 				return {
/******/ 					chain: [id],
/******/ 					id: id
/******/ 				};
/******/ 			});
/******/ 			while(queue.length > 0) {
/******/ 				var queueItem = queue.pop();
/******/ 				var moduleId = queueItem.id;
/******/ 				var chain = queueItem.chain;
/******/ 				module = installedModules[moduleId];
/******/ 				if(!module || module.hot._selfAccepted)
/******/ 					continue;
/******/ 				if(module.hot._selfDeclined) {
/******/ 					return {
/******/ 						type: "self-declined",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				if(module.hot._main) {
/******/ 					return {
/******/ 						type: "unaccepted",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				for(var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if(!parent) continue;
/******/ 					if(parent.hot._declinedDependencies[moduleId]) {
/******/ 						return {
/******/ 							type: "declined",
/******/ 							chain: chain.concat([parentId]),
/******/ 							moduleId: moduleId,
/******/ 							parentId: parentId
/******/ 						};
/******/ 					}
/******/ 					if(outdatedModules.indexOf(parentId) >= 0) continue;
/******/ 					if(parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if(!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push({
/******/ 						chain: chain.concat([parentId]),
/******/ 						id: parentId
/******/ 					});
/******/ 				}
/******/ 			}
/******/ 	
/******/ 			return {
/******/ 				type: "accepted",
/******/ 				moduleId: updateModuleId,
/******/ 				outdatedModules: outdatedModules,
/******/ 				outdatedDependencies: outdatedDependencies
/******/ 			};
/******/ 		}
/******/ 	
/******/ 		function addAllToSet(a, b) {
/******/ 			for(var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if(a.indexOf(item) < 0)
/******/ 					a.push(item);
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/ 	
/******/ 		var warnUnexpectedRequire = function warnUnexpectedRequire() {
/******/ 			console.warn("[HMR] unexpected require(" + result.moduleId + ") to disposed module");
/******/ 		};
/******/ 	
/******/ 		for(var id in hotUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				moduleId = toModuleId(id);
/******/ 				var result;
/******/ 				if(hotUpdate[id]) {
/******/ 					result = getAffectedStuff(moduleId);
/******/ 				} else {
/******/ 					result = {
/******/ 						type: "disposed",
/******/ 						moduleId: id
/******/ 					};
/******/ 				}
/******/ 				var abortError = false;
/******/ 				var doApply = false;
/******/ 				var doDispose = false;
/******/ 				var chainInfo = "";
/******/ 				if(result.chain) {
/******/ 					chainInfo = "\nUpdate propagation: " + result.chain.join(" -> ");
/******/ 				}
/******/ 				switch(result.type) {
/******/ 					case "self-declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of self decline: " + result.moduleId + chainInfo);
/******/ 						break;
/******/ 					case "declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of declined dependency: " + result.moduleId + " in " + result.parentId + chainInfo);
/******/ 						break;
/******/ 					case "unaccepted":
/******/ 						if(options.onUnaccepted)
/******/ 							options.onUnaccepted(result);
/******/ 						if(!options.ignoreUnaccepted)
/******/ 							abortError = new Error("Aborted because " + moduleId + " is not accepted" + chainInfo);
/******/ 						break;
/******/ 					case "accepted":
/******/ 						if(options.onAccepted)
/******/ 							options.onAccepted(result);
/******/ 						doApply = true;
/******/ 						break;
/******/ 					case "disposed":
/******/ 						if(options.onDisposed)
/******/ 							options.onDisposed(result);
/******/ 						doDispose = true;
/******/ 						break;
/******/ 					default:
/******/ 						throw new Error("Unexception type " + result.type);
/******/ 				}
/******/ 				if(abortError) {
/******/ 					hotSetStatus("abort");
/******/ 					return Promise.reject(abortError);
/******/ 				}
/******/ 				if(doApply) {
/******/ 					appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 					addAllToSet(outdatedModules, result.outdatedModules);
/******/ 					for(moduleId in result.outdatedDependencies) {
/******/ 						if(Object.prototype.hasOwnProperty.call(result.outdatedDependencies, moduleId)) {
/******/ 							if(!outdatedDependencies[moduleId])
/******/ 								outdatedDependencies[moduleId] = [];
/******/ 							addAllToSet(outdatedDependencies[moduleId], result.outdatedDependencies[moduleId]);
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 				if(doDispose) {
/******/ 					addAllToSet(outdatedModules, [result.moduleId]);
/******/ 					appliedUpdate[moduleId] = warnUnexpectedRequire;
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for(i = 0; i < outdatedModules.length; i++) {
/******/ 			moduleId = outdatedModules[i];
/******/ 			if(installedModules[moduleId] && installedModules[moduleId].hot._selfAccepted)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/ 	
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		Object.keys(hotAvailableFilesMap).forEach(function(chunkId) {
/******/ 			if(hotAvailableFilesMap[chunkId] === false) {
/******/ 				hotDisposeChunk(chunkId);
/******/ 			}
/******/ 		});
/******/ 	
/******/ 		var idx;
/******/ 		var queue = outdatedModules.slice();
/******/ 		while(queue.length > 0) {
/******/ 			moduleId = queue.pop();
/******/ 			module = installedModules[moduleId];
/******/ 			if(!module) continue;
/******/ 	
/******/ 			var data = {};
/******/ 	
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for(j = 0; j < disposeHandlers.length; j++) {
/******/ 				cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/ 	
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/ 	
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/ 	
/******/ 			// when disposing there is no need to call dispose handler
/******/ 			delete outdatedDependencies[moduleId];
/******/ 	
/******/ 			// remove "parents" references from all children
/******/ 			for(j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if(!child) continue;
/******/ 				idx = child.parents.indexOf(moduleId);
/******/ 				if(idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// remove outdated dependency from module children
/******/ 		var dependency;
/******/ 		var moduleOutdatedDependencies;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					for(j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 						dependency = moduleOutdatedDependencies[j];
/******/ 						idx = module.children.indexOf(dependency);
/******/ 						if(idx >= 0) module.children.splice(idx, 1);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/ 	
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/ 	
/******/ 		// insert new code
/******/ 		for(moduleId in appliedUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					var callbacks = [];
/******/ 					for(i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 						dependency = moduleOutdatedDependencies[i];
/******/ 						cb = module.hot._acceptedDependencies[dependency];
/******/ 						if(cb) {
/******/ 							if(callbacks.indexOf(cb) >= 0) continue;
/******/ 							callbacks.push(cb);
/******/ 						}
/******/ 					}
/******/ 					for(i = 0; i < callbacks.length; i++) {
/******/ 						cb = callbacks[i];
/******/ 						try {
/******/ 							cb(moduleOutdatedDependencies);
/******/ 						} catch(err) {
/******/ 							if(options.onErrored) {
/******/ 								options.onErrored({
/******/ 									type: "accept-errored",
/******/ 									moduleId: moduleId,
/******/ 									dependencyId: moduleOutdatedDependencies[i],
/******/ 									error: err
/******/ 								});
/******/ 							}
/******/ 							if(!options.ignoreErrored) {
/******/ 								if(!error)
/******/ 									error = err;
/******/ 							}
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Load self accepted modules
/******/ 		for(i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch(err) {
/******/ 				if(typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch(err2) {
/******/ 						if(options.onErrored) {
/******/ 							options.onErrored({
/******/ 								type: "self-accept-error-handler-errored",
/******/ 								moduleId: moduleId,
/******/ 								error: err2,
/******/ 								orginalError: err, // TODO remove in webpack 4
/******/ 								originalError: err
/******/ 							});
/******/ 						}
/******/ 						if(!options.ignoreErrored) {
/******/ 							if(!error)
/******/ 								error = err2;
/******/ 						}
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				} else {
/******/ 					if(options.onErrored) {
/******/ 						options.onErrored({
/******/ 							type: "self-accept-errored",
/******/ 							moduleId: moduleId,
/******/ 							error: err
/******/ 						});
/******/ 					}
/******/ 					if(!options.ignoreErrored) {
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if(error) {
/******/ 			hotSetStatus("fail");
/******/ 			return Promise.reject(error);
/******/ 		}
/******/ 	
/******/ 		hotSetStatus("idle");
/******/ 		return new Promise(function(resolve) {
/******/ 			resolve(outdatedModules);
/******/ 		});
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {},
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: (hotCurrentParentsTemp = hotCurrentParents, hotCurrentParents = [], hotCurrentParentsTemp),
/******/ 			children: []
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire("./src/js/main.js")(__webpack_require__.s = "./src/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/component/cabinet__generateMyData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var cabinet_generateMyData = function () {
    function cabinet_generateMyData(id, count) {
        _classCallCheck(this, cabinet_generateMyData);

        this.id = id;
        this.count = count;
    }

    _createClass(cabinet_generateMyData, [{
        key: 'init_area_GraphJSON',
        value: function init_area_GraphJSON() {
            var $thisID = this.id;
            var $thisCount = this.count;

            $.getJSON('/request', { action: 'getGraph', key: this.id, count: $thisCount }, function (data) {
                Highcharts.chart($thisID, {
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    // subtitle: {
                    //     text: document.ontouchstart === undefined ?
                    //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                    // },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [[0, '#c8c2f3'], [1, 'rgba(255, 255, 255, 0.92)']]
                            },
                            marker: {
                                radius: 2
                            },
                            lineColor: '#4791cf',
                            lineWidth: 2,
                            states: {
                                // hover: {
                                //     lineWidth: 1
                                // }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: $('#' + $thisID).data('lng__name-grap'),
                        data: data.data
                    }]
                });
            });
        }
    }, {
        key: 'init_area_GraphJSON_complexity',
        value: function init_area_GraphJSON_complexity(cur) {
            var $thisID = this.id;
            var $thisCount = this.count;
            var $thisCur = cur;

            $.getJSON('/request', { action: 'getGraph', key: this.id, count: $thisCount }, function (data) {
                var chart = $('#' + $thisID).highcharts();
                var arrayGraph = [];

                data.data.map(function (item, i) {
                    if (item.curName == $thisCur) {
                        arrayGraph = item.graphData;
                    }
                });
                Highcharts.chart($thisID, {
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    // subtitle: {
                    //     text: document.ontouchstart === undefined ?
                    //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                    // },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [[0, '#c8c2f3'], [1, 'rgba(255, 255, 255, 0.92)']]
                            },
                            marker: {
                                radius: 2
                            },
                            lineColor: '#4791cf',
                            lineWidth: 2,
                            states: {
                                // hover: {
                                //     lineWidth: 1
                                // }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: $('#' + $thisID).data('lng__name-grap'),
                        data: arrayGraph
                    }]
                });
            });
        }
    }, {
        key: 'init_area_GraphJSON_cluster',
        value: function init_area_GraphJSON_cluster() {
            var $thisID = this.id;
            var $thisCount = this.count;

            $.getJSON('/request', { action: 'getGraph', key: this.id, count: $thisCount }, function (data) {
                Highcharts.chart($thisID, {
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    // subtitle: {
                    //     text: document.ontouchstart === undefined ?
                    //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                    // },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [[0, '#c8c2f3'], [1, 'rgba(255, 255, 255, 0.92)']]
                            },
                            marker: {
                                radius: 2
                            },
                            lineColor: '#4791cf',
                            lineWidth: 2,
                            states: {
                                // hover: {
                                //     lineWidth: 1
                                // }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: $('#' + $thisID).data('lng__name-grap'),
                        data: data.data
                    }]
                });
            });
        }
    }, {
        key: 'showLoad',
        value: function showLoad() {
            var chart = $('#' + this.id).highcharts();
            console.log(chart);
            chart.showLoading('Loading...');
        }
    }, {
        key: 'update_date',
        value: function update_date(cur) {
            var $thisID = this.id;
            var $thisCount = this.count;
            var $thisCur = cur;

            $.getJSON('/request', { action: 'getGraph', key: this.id, count: $thisCount }, function (data) {
                var chart = $('#' + $thisID).highcharts();
                var arrayGraph = [];

                if ($thisCur != 'all') {

                    while (chart.series.length > 0) {
                        chart.series[0].remove(true);
                    }

                    data.data.map(function (item, i) {
                        if (item.curName == $thisCur) {
                            arrayGraph = item.graphData;

                            chart.addSeries({
                                name: item.curName,
                                data: item.graphData
                            });
                        }
                    });
                } else {
                    chart.series[0].remove(false);

                    data.data.map(function (item, i) {
                        chart.addSeries({
                            name: item.curName,
                            data: item.graphData
                        });
                    });

                    chart.redraw();
                }

                $('.complexity-of-mining .wrapp', $('#personal_cabinet')).removeClass('loading');
                chart.hideLoading();
            });
        }
    }, {
        key: 'change_show_point',
        value: function change_show_point(new_count) {
            var $thisID = this.id;
            var $new_count = new_count;

            $.getJSON('/request', { action: 'getGraph', key: this.id, count: new_count }, function (data) {
                Highcharts.chart($thisID, {
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: ''
                    },
                    // subtitle: {
                    //     text: document.ontouchstart === undefined ?
                    //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                    // },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [[0, '#c8c2f3'], [1, 'rgba(255, 255, 255, 0.92)']]
                            },
                            marker: {
                                radius: 2
                            },
                            lineColor: '#4791cf',
                            lineWidth: 2,
                            states: {
                                // hover: {
                                //     lineWidth: 1
                                // }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: $('#' + $thisID).data('lng__name-grap'),
                        data: data.data
                    }]
                });
            });
        }
    }]);

    return cabinet_generateMyData;
}();

exports.cabinet_generateMyData = cabinet_generateMyData;

/***/ }),

/***/ "./src/js/component/dropdown.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var dropdown = function () {
  function dropdown(obj) {
    _classCallCheck(this, dropdown);

    this.elem = obj.elem;
  }

  _createClass(dropdown, [{
    key: 'init_dropdown',
    value: function init_dropdown() {
      $(this.elem).select2({
        minimumResultsForSearch: Infinity, // disabled search
        containerCssClass: 'dd___base-container',
        dropdownCssClass: 'dd___base-dropdown',
        dropdownParent: $(this.elem).parent('div'),
        width: "100%"
      });
    }

    // triggerChange_dropdown() {
    //   $(this.elem).on('select2:open', function(e) {
    //     console.log( 'hi' );
    //   });
    // }

  }]);

  return dropdown;
}();

exports.default = dropdown;

/***/ }),

/***/ "./src/js/main.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _cabinet__generateMyData = __webpack_require__("./src/js/component/cabinet__generateMyData.js");

var _dropdown = __webpack_require__("./src/js/component/dropdown.js");

var _dropdown2 = _interopRequireDefault(_dropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var breakPointHeadMenu = 1300;
var breakPointDesktop = 1200;
var breakPoint1024 = 1024;
var breakPoint768 = 768;

$(function () {

  if ($('.pages-list').length != 0) {
    $('.pages-list').pagination({
      items: 100,
      itemsOnPage: 10,
      hrefTextPrefix: '#',
      cssStyle: 'pages-list',
      prevText: '1',
      nextText: '2',
      onPageClick: function onPageClick(pageNumber, event) {
        console.log(pageNumber);
      }
    });
  }

  //Graphs (START)
  if ($('#personal_cabinet').length > 0) {
    var myMining = new _cabinet__generateMyData.cabinet_generateMyData('cabinet__mining_process');
    myMining.init_area_GraphJSON();
  }

  if ($('#main_head').length > 0) {
    var avageHashRate = new _cabinet__generateMyData.cabinet_generateMyData('main-graph', 3);
    avageHashRate.init_area_GraphJSON();

    $('#main_head .head-grap .selector-grap span').click(function () {
      if ($(this).hasClass('active')) {
        return;
      } else {
        $('#main_head .head-grap .selector-grap span.active').removeClass('active');
        $(this).addClass('active');
      }

      avageHashRate.showLoad();
      avageHashRate.change_show_point($(this).data('point'));
    });
  }
  //Graphs (END)

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  initMobileMenu();
  //INIT SLICK (START)
  initSlickRoadmap();
  initSlickMedia();
  if ($(window).outerWidth() <= 768) {
    if ($('#our-document').length == 1) {
      initSlickOurDoc();
    }

    if ($('#our-advantages').length == 1) {
      initSlickOurAdv();
    }
  }

  if (document.getElementById("products-list-slick") != null) {
    slickProductPage();
    poductDetailImage();
  }
  //INIT SLICK (END)

  //INIT DROPDOWN (START)
  if (document.getElementById("cart") != null) {
    CART__selectDeliveryCity();
    CART__selectPayer();
    CART__selectPayerMethod();
  }

  if (document.getElementById("personal_cabinet") != null) {
    PCwalletSelecCurrency();

    $('.copy-link > a').click(function (e) {
      e.preventDefault();

      if ($(this).hasClass('process-copy')) {
        return;
      }

      var copyText = document.getElementById("ref-link");
      copyText.select();
      document.execCommand("copy");
      $(this).addClass('process-copy');

      setTimeout(function () {
        $('.copy-link > a').removeClass('process-copy');
      }, 3000);
    });

    $('.list-wallet .copy-wrapp').click(function () {

      if ($(this).hasClass('process-copy')) {
        return;
      }
      $(this).addClass('process-copy');

      var copyText = $(this).siblings('input');
      console.log('hi');
      copyText.select();
      document.execCommand("copy");
      $(this).attr('data-tooltip', $(this).data('lngcopydone'));
    });

    $('.list-wallet .copy-wrapp').mouseleave(function () {
      var $this_item = $(this);
      $(this).removeClass('process-copy');
      setTimeout(function () {
        $($this_item).attr('data-tooltip', $($this_item).data('lngcopy'));
      }, 400);
    });
  }
  //INIT DROPDOWN (END)

  // ALL MASK
  $('.counter-base input, .only-num, #g2fa_secret').inputmask({ "regex": "[0-9]*" });
  $('.wallet-mask').inputmask({ "regex": "[0-9a-zA-Z]*" });
  $('#g2fa_secret').inputmask({ "mask": "999 999" });

  modernInput();
  subscribeNews();
  showAllTeam();
  modalWindow();
  selectMediaAboutUs();
  playMainpageVideo();
  //Valid 
  validField();

  $(window).resize(function (e) {
    if ($(window).outerWidth() > 768) {
      if ($('#our-document').length == 1 && $('#our-document .doc-list').hasClass('slick-initialized')) {
        $('#our-document .doc-list').slick('unslick');
      }
      if ($('#our-advantages').length == 1 && $('#our-advantages .advantages-list').hasClass('slick-initialized')) {
        $('#our-advantages .advantages-list').slick('unslick');
      }
    } else {
      if ($('#our-document').length == 1 && !$('#our-document .doc-list').hasClass('slick-initialized')) {
        initSlickOurDoc();
      }
      if ($('#our-advantages').length == 1 && !$('#our-advantages .advantages-list').hasClass('slick-initialized')) {
        initSlickOurAdv();
      }
    }

    //Close menu if open
    if ($(window).outerWidth() > breakPointHeadMenu) {
      if ($('.btn__mob-menu').hasClass('open')) {
        $('.btn__mob-menu').removeClass('open');
        $('.main-site-menu').removeClass('showed');
        $('.blackout').removeAttr('style');
        $('.with-drop', $('header')).each(function () {
          $(this).removeClass('open');
        });
      }
    }

    if ($(window).outerWidth() < breakPointDesktop && $('section#personal_cabinet').length == 1) {
      personalCabinetMoveBlock(true);
    } else {
      personalCabinetMoveBlock(false);
    }
  });

  //if there is something on the page
  document.getElementById("contact-us") != null ? initContactMap() : '';
  document.getElementById("news") != null ? initDotsNews() : '';
  $('.countdown').length != 0 ? timerMainPage() : '';
  if (document.getElementById("personal_cabinet") != null) {
    initSlickStreamVideo();

    // create mobile complexity-mng
    $('#desktop-complexity-mng li').each(function (i, item) {
      var $this__cur = $(item).children('button').data('cur'),
          $this__name = $(item).find('span').text();

      $('#mobile-complexity-mng').append('<option value="' + $this__cur + '">' + $this__name + '</option>');
    });

    //Init dropdown mobile
    if ($('#mobile-complexity-mng option').length != 0) {
      initMobileComplexity($('#mobile-complexity-mng'));
      combineComplexityMining();
    }
  };

  if (document.getElementById("cloud-mining") != null) {
    initCalcInvestSlider();
    miningPlan();
    initDropDownMiningPlan();

    scrollToChooseMiningPlan();
  }

  if (document.getElementById("catalog") != null) {
    filterRangeInit();
  }

  if (document.getElementById("top-100") != null) {
    generateTOP100();
  }

  // Scroll HOW IT WORK
  $('.next-step').click(function () {
    var this_step = $(this);

    $('html, body').animate({
      scrollTop: $(this_step).parents('.step').next().offset().top
    });
  });

  // Couter add/remove
  $('.counter-base input').focusout(function () {
    if ($(this).val().trim() == 0) {
      $(this).val(1);
    }
  });

  $('.counter-base .circle').click(function () {
    var value_count = $(this).siblings('.input-base').find('input');

    if ($(this).hasClass('decrement') && $(value_count).val() != 1) {
      $(value_count).val(parseInt($(value_count).val()) - 1);
    } else if ($(this).hasClass('increment')) {
      $(value_count).val(parseInt($(value_count).val()) + 1);
    }
  });

  //INIT FANCYBOX GALERY ( START )
  if ($('[data-fancybox="gallery"]').length != 0) {
    $('[data-fancybox="gallery"]').fancybox({});
  }

  $('.see-galery .btn-long').click(function (e) {
    e.preventDefault();

    $.get('/request?action=getGallery', function (data) {
      $.fancybox.open(data.data);
    });
    // $.fancybox.open()
    // $('.wrapp-galery-img a').first().click();
  });
  //INIT FANCYBOX GALERY ( END )

  //Personal cabinet  PC == personal_cabinet
  PCwalletWork();
  editPersonalCabinet();

  if ($(window).outerWidth() < breakPointDesktop && $('section#personal_cabinet').length == 1) {
    personalCabinetMoveBlock(true);
  } else {
    personalCabinetMoveBlock(false);
  }

  //TABLE ANALYST (START)
  if (document.getElementById("analyst-data") != null) {
    checkMiningState();
    initDropTypeAlgorithm();
    initDropOwner();
    initDropListAnalyst();
  }

  //MODAL INIT (START)
  $('#payment-start').length != 0 ? initModalPayment() : '';

  // ASIC INIT (START)

  // ASIC INIT (END)
});

//MODAL (START)
function initModalPayment() {
  $("#payment-start").iziModal({
    background: null,
    width: 610,
    overlay: true,
    overlayColor: 'rgba(27,17,60,0.8)',
    // autoOpen: true,
    onClosed: function onClosed() {}
  });
}
//MODAL (END)

// ********** BASE FUNCTION ********** //  (START) 
var AnimateElement = function AnimateElement(elemParams) {
  var animateProps = elemParams.animateProps,
      elem = elemParams.elem,
      _elemParams$duration = elemParams.duration,
      duration = _elemParams$duration === undefined ? 150 : _elemParams$duration,
      _elemParams$queue = elemParams.queue,
      queue = _elemParams$queue === undefined ? false : _elemParams$queue,
      specialEasing = elemParams.specialEasing,
      funcStart = elemParams.funcStart,
      funcStep = elemParams.funcStep,
      funcProgress = elemParams.funcProgress,
      funcComplite = elemParams.funcComplite;


  $(elem).animate(animateProps, {
    specialEasing: specialEasing,
    queue: queue,
    duration: duration,
    start: funcStart,
    step: funcStep,
    progress: funcProgress,
    complete: funcComplite
  });
};

function checkAddErrorClass(field, className, error) {

  if (error) {
    $(field).addClass(className);
  } else {
    $(field).removeClass(className);
  }
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function checkAnyPhone(phone) {
  var reg = /^[0-9\-\+\(\)]{6,15}$/;
  return reg.test(phone);
}

function checkName(name) {
  var reg = /[a-zA-Z]{1,255}$/;
  return reg.test(name);
}

function checkNumber(str) {
  var reg = /[0-9]$/;
  return reg.test(str);
}

function checkNameENRU(name) {
  var reg = /[a-zA-Zа-яА-Я\-]{1,255}$/;
  return reg.test(name);
}

var validFiledRules = {
  name: function name(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {

      if (!checkName(str.trim())) {
        $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
        checkAddErrorClass($(input), 'error', true);

        return true; // true -- if field has error
      } else {
        checkAddErrorClass($(input), 'error', false);
      }
    }
  },

  nameENRU: function nameENRU(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {

      if (!checkNameENRU(str.trim())) {
        $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
        checkAddErrorClass($(input), 'error', true);

        return true; // true -- if field has error
      } else {
        checkAddErrorClass($(input), 'error', false);
      }
    }
  },

  email: function email(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {

      if (!validateEmail(str.trim())) {
        $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
        checkAddErrorClass($(input), 'error', true);

        return true; // true -- if field has error
      } else {
        checkAddErrorClass($(input), 'error', false);
      }
    }
  },

  phone: function phone(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {

      if (!checkAnyPhone(str.trim())) {
        $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
        checkAddErrorClass($(input), 'error', true);

        return true; // true -- if field has error
      } else {
        checkAddErrorClass($(input), 'error', false);
      }
    }
  },

  textarea: function textarea(str, input) {
    if (str.trim().length == 0) {
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {
      checkAddErrorClass($(input), 'error', false);
    }
  },

  address: function address(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
      checkAddErrorClass($(input), 'error', false);
    }
  },

  checkbox: function checkbox(check, input) {
    if (!check) {
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {
      checkAddErrorClass($(input), 'error', false);
    }
  },

  password: function password(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {
      checkAddErrorClass($(input), 'error', false);
    }
  },

  compareEmail: function compareEmail(str1, str2, input1, input2) {
    if ($(input1).hasClass('error') || $(input2).hasClass('error')) {
      return;
    }

    if (str1 !== str2) {
      $(input1).siblings('.this-tooltip').attr('data-tooltip', $(input1).data('error-compare'));
      $(input2).siblings('.this-tooltip').attr('data-tooltip', $(input2).data('error-compare'));
      checkAddErrorClass($(input1), 'error', true);
      checkAddErrorClass($(input2), 'error', true);

      return true; // true -- if field has error
    } else {
      checkAddErrorClass($(input1), 'error', false);
      checkAddErrorClass($(input2), 'error', false);
    }
  },

  comparePassword: function comparePassword(str1, str2, input1, input2) {
    if ($(input1).hasClass('error') || $(input2).hasClass('error')) {
      return;
    }

    if (str1 !== str2) {
      $(input1).siblings('.this-tooltip').attr('data-tooltip', $(input1).data('error-compare'));
      $(input2).siblings('.this-tooltip').attr('data-tooltip', $(input2).data('error-compare'));
      checkAddErrorClass($(input1), 'error', true);
      checkAddErrorClass($(input2), 'error', true);

      return true; // true -- if field has error
    } else {
      checkAddErrorClass($(input1), 'error', false);
      checkAddErrorClass($(input2), 'error', false);
    }
  },

  secretCode: function secretCode(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);
      console.log('hi');

      return true; // true -- if field has error
    } else {

      if (!checkNumber(str.trim()) || str.trim().length != 7) {
        $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
        checkAddErrorClass($(input), 'error', true);

        return true; // true -- if field has error
      } else {
        checkAddErrorClass($(input), 'error', false);
      }
    }
  },

  walletNumber: function walletNumber(str, input) {
    if (str.trim().length == 0) {
      $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-empty'));
      checkAddErrorClass($(input), 'error', true);

      return true; // true -- if field has error
    } else {

      if (!checkNumber(str.trim())) {
        $(input).siblings('.this-tooltip').attr('data-tooltip', $(input).data('error-format'));
        checkAddErrorClass($(input), 'error', true);

        return true; // true -- if field has error
      } else {
        checkAddErrorClass($(input), 'error', false);
      }
    }
  }
};

var modalWindow = function modalWindow() {
  $('a[href="#modal"]').click(function (e) {
    var modal__target = $(this).data('modal');
    e.preventDefault();

    // $('.blackout-modal').css('display','block')

    // IF CLICK OUTSIDE (START)  *******  INIT 
    $(modal__target).mousedown(function (e) {
      var div = $(modal__target + ' .modal__content'); // ID or class element
      var menuButton = $(modal__target + ' .modal__content .close-popup');
      if (!div.is(e.target) // if the click was outside the element
      && div.has(e.target).length === 0 && !menuButton.is(e.target)) {
        // and not by it's child elements
        $(modal__target + ' .close-popup').click();
        $(modal__target).unbind('mousedown');
      }
    });
    // IF CLICK OUTSIDE (END) *******  INIT

    AnimateElement({
      elem: $(modal__target + ' .modal__content'),
      duration: 300,
      animateProps: {
        opacity: 1
      },
      funcStart: function funcStart() {
        $(this).addClass('in__wr');
        $(modal__target).addClass('open');
      }
    });

    if ($(this).attr('data-modal') == '#auth-popup') {
      var modal_show = $(this).data('auth');

      $(modal__target + ' .state').each(function (i, item) {
        $(this).removeClass('show end');
      });
      $(modal__target + " " + modal_show).addClass('show end');
    }

    $(modal__target + ' .close-popup').click(function () {
      AnimateElement({
        elem: $(modal__target + ' .modal__content'),
        duration: 300,
        animateProps: {
          opacity: 0
        },
        funcStart: function funcStart() {
          $(this).removeClass('in__wr');

          AnimateElement({
            queue: false,
            elem: $('.blackout-modal'),
            animateProps: {
              opacity: 0
            }
          });
        },
        funcComplite: function funcComplite() {
          $('.blackout-modal').removeAttr('style');
          $(modal__target).removeAttr('style').removeClass('open');
          $(this).removeAttr('style');
        }
      });
    });
  });

  //CHANGE INSIDE AUTH POPUP
  $('#auth-popup span.cng-pop').click(function () {
    $('#auth-popup .state').each(function (i, item) {
      $(this).removeClass('show end').removeAttr('style');
    });

    $('#auth-popup ' + $(this).data('change')).addClass('show');

    AnimateElement({
      elem: $('#auth-popup ' + $(this).data('change')),
      duration: 300,
      animateProps: {
        opacity: 1
      },
      funcComplite: function funcComplite() {
        $(this).addClass('end');
      }
    });
  });
};

var calculateCloudMining = {
  calc: function calc() {
    var $PARENT_ELEMENT = $('#cloud-mining'),
        _thisValue = $('#calc-invest', $PARENT_ELEMENT).slider('option', 'value'),
        _thisCurrency = $('.data-calculator .list-currency button.active', $PARENT_ELEMENT).data('cur');

    // $.get('/request', { currency: _thisCurrency, amount: _thisValue })
    //   .done(function() {
    //     alert( "second success" );
    //   })
    //   .fail(function() {
    //     alert( "error" );
    //   })
  },
  getResult: function getResult() {}
  // ********** BASE FUNCTION ********** //  (START) 

  //SLICK (START)
  // ***** Company Roadmap ***** (START)
};var initSlickRoadmap = function initSlickRoadmap() {
  $('#company-roadmap .wrapp-com-road').slick({
    dots: false,
    draggable: false,
    variableWidth: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>',
    responsive: [{
      breakpoint: 1440,
      settings: {
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        variableWidth: false
      }
    }, {
      breakpoint: 1200,
      settings: _defineProperty({
        variableWidth: false,
        dots: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true
      }, 'variableWidth', false)
    }, {
      breakpoint: 768,
      settings: _defineProperty({
        variableWidth: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
      }, 'variableWidth', false)
    }]
  });
};
// ***** Company Roadmap ***** (END)

// ***** Media About us ***** (START) 
var initSlickMedia = function initSlickMedia() {
  $('#media-about-us .media-slick').slick({
    dots: false,
    draggable: false,
    variableWidth: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        variableWidth: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
};
// ***** Media About us ***** (END)

// ***** Our Doc ***** (START)
var initSlickOurDoc = function initSlickOurDoc() {
  $('#our-document .doc-list').slick({
    dots: true,
    draggable: false,
    variableWidth: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>'
  });
};
// ***** Our Doc ***** (END)

// ***** Our Advantages ***** (START)
var initSlickOurAdv = function initSlickOurAdv() {
  $('#our-advantages .advantages-list').slick({
    dots: true,
    draggable: false,
    variableWidth: false,
    vertical: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    verticalSwiping: false,
    adaptiveHeight: true
    // prevArrow: '<button type="button" class="slick-prev slick-right"><img src="../images/icon_slick_left.svg"></button>',
    // nextArrow: '<button type="button" class="slick-next slick-left"><img src="../images/icon_slick_right.svg"></button>',
  });
};
// ***** Our Advantages ***** (END)

// ***** Stream video ***** (START)
var initSlickStreamVideo = function initSlickStreamVideo() {
  $('#personal_cabinet .wrapp-stream-video').slick({
    dots: false,
    draggable: false,
    variableWidth: false,
    vertical: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>'
  });
};
// ***** Stream video ***** (END)

// ***** Product LIST ***** (START)
var slickProductPage = function slickProductPage() {
  $('.products', '#products-list-slick').slick({
    dots: false,
    draggable: false,
    variableWidth: false,
    vertical: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>',
    responsive: [{
      breakpoint: breakPoint1024,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: breakPoint768,
      settings: {
        slidesToShow: 1
      }
    }]
  });
};
// ***** Product LIST ***** (END)

// ***** Product IMAGE ***** (START)
var poductDetailImage = function poductDetailImage() {
  $('.all-image', '#product-detail').slick({
    dots: false,
    draggable: false,
    // variableWidth: false,
    vertical: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: false,
    verticalSwiping: false,
    adaptiveHeight: false,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>',
    responsive: [{
      breakpoint: 1024,
      settings: {
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        vertical: false,
        variableWidth: false
      }
    }]
  });
};

$('.all-image', '#product-detail').on('init', function (slick) {
  var mainImage = $(this);

  $('.image', $(this)).click(function () {

    if ($(this).hasClass('active')) {
      return false;
    }

    var $image = $(this).find('img').attr('src');

    $(mainImage).find('.image').each(function (i, item) {
      $(item).removeClass('active');
    });

    $(this).addClass('active');
    $('.image-fullsize .wrapp-image img', $('#product-detail')).attr('src', $image);
  });
});
// ***** Product IMAGE ***** (END)

// ***** Top Cryptocurrencies ***** (START)
var topCryptocurrencies = function topCryptocurrencies() {
  $('.wrapp-top-100', '#personal_cabinet').slick({
    dots: false,
    draggable: false,
    // variableWidth: false,
    vertical: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    verticalSwiping: false,
    adaptiveHeight: false,
    prevArrow: '<button type="button" class="slick-prev slick-right"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M9 4.5a.731.731 0 0 0-.732-.737h-5.77L4.982 1.26a.736.736 0 0 0 0-1.042.723.723 0 0 0-1.034 0L.214 3.978A.748.748 0 0 0 0 4.5c0 .186.074.38.214.521l3.734 3.761c.288.29.746.29 1.034 0a.736.736 0 0 0 0-1.042L2.498 5.237h5.77A.731.731 0 0 0 9 4.5z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-left"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"><path fill="#4A84CE" fill-rule="nonzero" d="M0 4.5c0 .41.325.737.732.737h5.77L4.018 7.74a.736.736 0 0 0 0 1.042c.288.29.746.29 1.034 0l3.734-3.76A.748.748 0 0 0 9 4.5a.748.748 0 0 0-.214-.521L5.052.218a.723.723 0 0 0-1.034 0 .736.736 0 0 0 0 1.042l2.484 2.503H.732A.731.731 0 0 0 0 4.5z"/></svg></button>'
  });
};
// ***** Top Cryptocurrencies ***** (END)
//SLICK (END)

//DROPDOWN INIT (START)
var CART__selectDeliveryCity = function CART__selectDeliveryCity() {
  var selectDeliveryCity = new _dropdown2.default({ elem: $('#cart__select-deliv-town') });
  selectDeliveryCity.init_dropdown();
};

var CART__selectPayer = function CART__selectPayer() {
  var selectPayer = new _dropdown2.default({ elem: $('#cart__selectPayer') });
  selectPayer.init_dropdown();
};

var CART__selectPayerMethod = function CART__selectPayerMethod() {
  var selectPayerMethod = new _dropdown2.default({ elem: $('#cart__selectPayerMethod') });
  selectPayerMethod.init_dropdown();
};

var PCwalletSelecCurrency = function PCwalletSelecCurrency() {
  var PC_select_currency = new _dropdown2.default({ elem: $('.pc-wallet') });
  PC_select_currency.init_dropdown();
};

var initDropDownMiningPlan = function initDropDownMiningPlan() {
  var CM_chooseMiningPlan = new _dropdown2.default({ elem: $('#mining-plan') });
  CM_chooseMiningPlan.init_dropdown();

  $('#mining-plan').on('select2:select', function (e) {
    $('.choose-currency .item[data-cur="' + e.params.data.id + '"]').click();
  });
};

var initDropTypeAlgorithm = function initDropTypeAlgorithm() {
  $('#analyst-data-type-algorithm').select2({
    minimumResultsForSearch: Infinity, // disabled search
    containerCssClass: 'dd___base-container',
    dropdownCssClass: 'dd___base-dropdown',
    dropdownParent: $('#analyst-data-type-algorithm').parent('div'),
    width: "100%",
    placeholder: $('#analyst-data-type-algorithm').data('placeholder')
  });
};

var initDropOwner = function initDropOwner() {
  $('#analyst-data-owner').select2({
    minimumResultsForSearch: Infinity, // disabled search
    containerCssClass: 'dd___base-container',
    dropdownCssClass: 'dd___base-dropdown',
    dropdownParent: $('#analyst-data-owner').parent('div'),
    width: "100%",
    placeholder: $('#analyst-data-owner').data('placeholder')
  });
};

var initDropListAnalyst = function initDropListAnalyst() {
  $('#analyst-data-cnt-list').select2({
    minimumResultsForSearch: Infinity, // disabled search
    containerCssClass: 'dd___base-container',
    dropdownCssClass: 'dd___base-dropdown',
    dropdownParent: $('#analyst-data-cnt-list').parent('div'),
    width: "117px"
  });
};
//DROPDOWN INIT (END)

var initMobileMenu = function initMobileMenu() {
  $('header .btn__mob-menu').click(function () {
    var height_menu = $('.main-site-menu', $('header')).height(),
        this_btn = $(this);

    if ($(this).hasClass('animate')) {
      return false;
    }

    function toggleMenu(height, btn, state) {
      if (state) {
        $('.blackout').css({ 'display': 'block' });
      }
      AnimateElement({
        elem: $('.main-site-menu', $('header')),
        animateProps: {
          height: state == true ? height : 0
        },
        duration: 300,
        funcStart: function funcStart() {
          $(btn).addClass('animate');

          if (state == true) {
            $('.blackout').css({ 'opacity': 0.5 });
          } else {
            $('.blackout').css({ 'opacity': 0 });
            $(btn).addClass('close');
          }
        },
        funcComplite: function funcComplite() {
          $(btn).removeClass('animate');

          if (state == true) {
            $(btn).addClass('open');
            $(this).addClass('showed');
            $(this).removeAttr('style');
          } else {
            $(btn).removeClass('open close');
            $('.blackout').removeAttr('style');
            $(this).removeClass('showed');
            $(this).removeAttr('style');
          }
        }
      });
    }

    if (!$(this).hasClass('open')) {
      $('.main-site-menu', $('header')).css({ 'display': 'block', 'height': '0px' });
      toggleMenu(height_menu, $(this_btn), true);
    } else {
      toggleMenu(height_menu, $(this_btn), false);
    }
  });

  $('header .main-site-menu li.with-drop .link').click(function () {
    var height_submenu = $(this).siblings('.submenu').height(),
        this_btn = $(this).parent('li.with-drop');

    if ($(this).parent('li.with-drop').hasClass('animate') || $(window).outerWidth() > breakPointHeadMenu) {
      return false;
    }

    function toggleSubMenu(height, btn, elem, state) {
      AnimateElement({
        elem: $(elem),
        animateProps: {
          height: state == true ? height : 0
        },
        duration: 300,
        funcStart: function funcStart() {
          $(btn).addClass('animate');
        },
        funcComplite: function funcComplite() {
          $(btn).removeClass('animate');

          if (state == true) {
            $(btn).addClass('open');
            $(elem).removeAttr('style');
          } else {
            $(btn).removeClass('open');
            $(elem).removeAttr('style');
          }
        }
      });
    }

    if (!$(this).parent('li.with-drop').hasClass('open')) {
      $(this).siblings('.submenu').css({ 'display': 'block', 'height': '0px' });
      toggleSubMenu(height_submenu, $(this_btn), $(this).siblings('.submenu'), true);
    } else {
      toggleSubMenu(height_submenu, $(this_btn), $(this).siblings('.submenu'), false);
    }
  });
};

var initContactMap = function initContactMap() {
  var mapContact, markerContact;
  var location = { lat: 55.6819426, lng: 37.6151809 };
  var center768 = { lat: 55.6805639, lng: 37.6150267 };
  var center = { lat: 55.6822408, lng: 37.6196879 };
  mapContact = new google.maps.Map(document.getElementById('contact-map'), {
    center: window.innerWidth > 768 ? center : center768,
    zoom: 16,
    disableDefaultUI: true
  });
  markerContact = new google.maps.Marker({
    position: location,
    map: mapContact,
    icon: '/images/icon-label-map.svg'
  });

  google.maps.event.addDomListener(window, 'resize', function (e) {
    if (window.innerWidth > 768) {
      mapContact.setCenter(center);
    } else {
      mapContact.setCenter(center768);
    }
  });
};

var initDotsNews = function initDotsNews() {

  $("#news .size-50.in-2 div.text > div").dotdotdot({
    ellipsis: '\u2026 ',
    height: 216
    // callback: function() {
    //   console.log('hi');
    // },
  });

  $("#news .size-50.size-full .text-news").dotdotdot({
    ellipsis: '\u2026 ',
    height: 44
  });
};

var timerMainPage = function timerMainPage() {
  var cnt_sec = $('.countdown .sec .num span'),
      cnt_min = $('.countdown .min .num span'),
      cnt_hrs = $('.countdown .hours .num span'),
      cnt_day = $('.countdown .days .num span');

  function timer() {

    var ss = parseInt($(cnt_sec).text());
    var mm = parseInt($(cnt_min).text());
    var hh = parseInt($(cnt_hrs).text());
    var dd = parseInt($(cnt_day).text());
    var end = false;

    if (ss > 0) {
      ss--;
    } else {
      ss = 59;

      if (mm > 0) {
        mm--;
      } else {
        mm = 59;

        if (hh > 0) {
          hh--;
        } else {
          hh = 23;

          if (dd > 0) {
            dd--;
          } else {
            end = true;
          }
        }
      }

      if (ss > 0) {
        ss--;
      } else {
        end = true;
      }
    }

    if (end) {
      clearInterval(intervalCallback);
    } else {

      ss < 10 ? $(cnt_sec).text("0" + ss) : $(cnt_sec).text(ss);
      mm < 10 ? $(cnt_min).text("0" + mm) : $(cnt_min).text(mm);
      hh < 10 ? $(cnt_hrs).text("0" + hh) : $(cnt_hrs).text(hh);
      $(cnt_day).text(dd);
    }
  }
  window.intervalCallback = setInterval(timer, 1000);
};

function statusPayment() {
  var dataSec = +$('.status-refresh span').text();

  function refreshPayment() {
    var end = false;

    if (dataSec > 0) {
      dataSec--;
    } else {
      end = true;
    }

    if (end) {
      clearInterval(refreshCallback);
    } else {

      $('.status-refresh span').text(dataSec);
    }
  }
  window.refreshCallback = setInterval(refreshPayment, 1000);
}

statusPayment();

var modernInput = function modernInput() {

  $('.input-modern input').focus(function () {
    var get_family = $(this).parents('.input-modern');

    $(get_family).addClass('focus');
  });

  $('.input-modern input').focusout(function () {
    var get_family = $(this).parents('.input-modern');

    $(get_family).removeClass('focus');

    if ($(this).val().trim().length == 0) {
      $(get_family).removeClass('enter');
    } else {
      $(get_family).addClass('enter');
    }
  });

  // $('input').focusout();
};

var subscribeNews = function subscribeNews() {

  $('.sub-mail button[type="submit"]').click(function (e) {
    e.preventDefault();

    var btn_sub = $(this),
        errorSubs = false;

    validFiledRules.email($(this).siblings('input#mail_sub').val(), $(this).parents('.input-modern')) ? errorSubs = true : '';

    if ($(btn_sub).hasClass('sendStart') || errorSubs) {
      return false;
    }

    $(this).addClass('sendStart');
    $('#mail_sub').focusout();

    $.post("/request", { action: 'subscribe', email: $('#mail_sub').val() }).done(function (data) {
      var request = JSON.parse(data);
      if (request.status == true) {
        $(btn_sub).removeClass('sendStart').addClass('done');
        $(btn_sub).parents('.input-modern').addClass('done');

        $('.sub-mail .input-modern').off('click');
        $(btn_sub).off('click');
        $(btn_sub).attr('onclick', 'event.preventDefault();');
      } else {
        $('.sub-mail button[type="submit"]').removeClass('sendStart');
      }
    }).fail(function () {
      $('.sub-mail button[type="submit"]').removeClass('sendStart');
    });
  });
};

var showAllTeam = function showAllTeam() {

  $('.show-all-team').click(function () {
    var visible_person = $('.list-team .item:visible', $('#our-team')),
        person_count = $('.list-team .item', $('#our-team')).length,
        count = person_count > 2 ? person_count - visible_person : 0;

    AnimateElement({
      elem: $('.list-team', $('#our-team')),
      animateProps: {
        height: $('.list-team .item', $('#our-team')).height() * person_count
      },
      duration: 300,
      funcStart: function funcStart() {
        $('.list-team').addClass('show');
      },
      funcComplite: function funcComplite() {
        $(this).removeAttr('style');
        $('.show-all-team').off('click');
      }
    });
  });
};

var initCalcInvestSlider = function initCalcInvestSlider() {

  $("#calc-invest").slider({
    range: 'min',
    min: 0,
    max: 10000,
    step: 1,
    // values: [ 0, 100000 ],
    classes: {
      "ui-slider": "fix-slider-aev",
      "ui-slider-handle": "fix-slider-handle-aev",
      "ui-slider-range": "fix-slider-range-aev"
    },
    create: function create(event, ui) {
      $(this).slider("option", "min", $(this).data('min'));
      $(this).slider("option", "max", $(this).data('max'));

      $('.range-select span').text($(this).data('min'));
      $('.typewriting-cost input').val($(this).data('min'));
    },
    change: function change(event, ui) {
      calculateCloudMining.calc();
    },
    slide: function slide(event, ui) {
      $('.range-select span', $('#cloud-mining')).text(ui.value);
      $('.typewriting-cost input', $('#cloud-mining')).val(ui.value);
    }
  });

  $("#calc-enterperise").slider({
    range: 'min',
    min: 1,
    max: 50,
    step: 1,
    // values: [ 0, 100000 ],
    classes: {
      "ui-slider": "fix-slider-aev",
      "ui-slider-handle": "fix-slider-handle-aev",
      "ui-slider-range": "fix-slider-range-aev"
    },
    create: function create(event, ui) {
      //set power
      $(this).siblings('.power').children('span').text(1);
      //set start cost
      $(this).siblings('.cost-plan').children('span').text(1 * parseInt($(this).data('cost')));
      $(this).slider("option", "max", $(this).data('power'));
    },
    change: function change(event, ui) {},
    slide: function slide(event, ui) {
      $(this).siblings('.power').children('span').text(ui.value);
      $(this).siblings('.cost-plan').children('span').text(ui.value * $(this).data('cost'));
    }
  });
};

var filterRangeInit = function filterRangeInit() {
  $("#range-power-comp").slider({
    range: 'min',
    min: 0,
    max: 0,
    step: 1,
    classes: {
      "ui-slider": "fix-slider-aev",
      "ui-slider-handle": "fix-slider-handle-aev",
      "ui-slider-range": "fix-slider-range-aev"
    },
    create: function create(event, ui) {
      $(this).slider("option", "min", $(this).data('min'));
      $(this).slider("option", "max", $(this).data('max'));
    },
    change: function change(event, ui) {},
    slide: function slide(event, ui) {}
  });

  $("#range-power-supp").slider({
    range: 'min',
    min: 0,
    max: 0,
    step: 1,
    classes: {
      "ui-slider": "fix-slider-aev",
      "ui-slider-handle": "fix-slider-handle-aev",
      "ui-slider-range": "fix-slider-range-aev"
    },
    create: function create(event, ui) {
      $(this).slider("option", "min", $(this).data('min'));
      $(this).slider("option", "max", $(this).data('max'));
    },
    change: function change(event, ui) {},
    slide: function slide(event, ui) {}
  });
};

var miningPlan = function miningPlan() {

  $('.choose-currency .item', $('#cloud-mining')).click(function () {
    var _thisCoin = $(this).data('coin');
    if ($(this).hasClass('disable') || $(this).hasClass('active')) {
      return;
    }
    //remove old active element
    $('.choose-currency .item.active', $('#cloud-mining')).removeClass('active');

    //add new active element
    $(this).addClass('active');

    //change in dropdown
    $('#mining-plan').val($(this).data('cur')).trigger('change');
  });
};

function playMainpageVideo() {
  $('.about-video .play-video').click(function () {
    $('.about-video').addClass('play-video');
    $('#videoBg').get(0).play();
  });
}

var validField = function validField() {

  //Send MSG  ===  Create Your Own Mining Complex
  $('.btn-snd', $('#create-complex')).click(function (e) {
    e.preventDefault();

    var parent_section = $('#create-complex'),
        cc_name = $('#cc_name', parent_section),
        cc_mail = $('#cc_mail', parent_section),
        cc_phone = $('#cc_phone', parent_section),
        cc_textF = $('#cc_text', parent_section),
        cc_agree = $('#cc_agreement', parent_section),
        errorSendMsg = false;

    validFiledRules.name($(cc_name).val(), $(cc_name)) ? errorSendMsg = true : '';
    validFiledRules.email($(cc_mail).val(), $(cc_mail)) ? errorSendMsg = true : '';
    validFiledRules.phone($(cc_phone).val(), $(cc_phone)) ? errorSendMsg = true : '';
    validFiledRules.textarea($(cc_textF).val(), $(cc_textF)) ? errorSendMsg = true : '';
    validFiledRules.checkbox($(cc_agree).prop('checked'), $(cc_agree)) ? errorSendMsg = true : '';

    if (errorSendMsg) {
      return false;
    }

    $('.loader', $('#create-complex')).addClass('show-loaded');

    $.post("/request", {
      action: "feedback",
      name: $(cc_name).val(),
      mail: $(cc_mail).val(),
      phone: $(cc_phone).val(),
      text: $(cc_textF).val()
    }, function (data) {
      var request = JSON.parse(data);
      if (request.status == true) {
        $('.info-block', $('#create-complex')).addClass('show-msg');
      } else {
        $('.loader', $('#create-complex')).removeClass('show-loaded');
      }
    });
  });

  $('.wr_btn-confirm-ord', $('#cart')).click(function (e) {
    e.preventDefault();

    var parent_section = $('#cart'),
        cart_fname = $('#cart_fname', parent_section),
        cart_lname = $('#cart_lname', parent_section),
        cart_phone = $('#cart_phone', parent_section),
        cart_mail = $('#cart_mail', parent_section),
        cart_addr = $('#cart_addr', parent_section),
        errorCart = false;

    validFiledRules.name($(cart_fname).val(), $(cart_fname)) ? errorCart = true : '';
    validFiledRules.name($(cart_lname).val(), $(cart_lname)) ? errorCart = true : '';
    validFiledRules.phone($(cart_phone).val(), $(cart_phone)) ? errorCart = true : '';
    validFiledRules.email($(cart_mail).val(), $(cart_mail)) ? errorCart = true : '';
    validFiledRules.address($(cart_addr).val(), $(cart_addr)) ? errorCart = true : '';

    console.log(errorCart);

    if (errorCart) {
      return false;
    }

    // else all OK, do ....
    // 
    // 
    //
  });

  //Auth VALID === POP UP
  $('.start-login .btn-popup', $('#auth-popup')).click(function (e) {
    e.preventDefault();

    var parent_section = $('#auth-popup'),
        auth_mail = $('#login-email', parent_section),
        auth_passwd = $('#login-passwd', parent_section),
        errorLogin = false;

    validFiledRules.email($(auth_mail).val(), $(auth_mail)) ? errorLogin = true : '';
    validFiledRules.password($(auth_passwd).val(), $(auth_passwd)) ? errorLogin = true : '';

    if ($('.wrapp-input.g2fa_secret', $(parent_section)).hasClass('show')) {
      validFiledRules.secretCode($(g2fa_secret).val(), $(g2fa_secret)) ? errorLogin = true : '';
    }

    console.log(errorLogin);

    if (errorLogin) {
      return false;
    }

    $('.loader', $('#auth-popup')).addClass('show-loaded');

    $.post("/request", {
      action: "login",
      email: $(auth_mail).val(),
      password: $(auth_passwd).val(),
      g2fa_secret: $(g2fa_secret).val()
    }, function (data) {
      var request = JSON.parse(data);

      if (request.status == true && request.g2fa == false) {
        window.location.replace('/lk');
      } else if (request.status == true && request.g2fa == true && request.secret_enter == false) {
        $('#login-email, #login-passwd').parents('div.wrapp-input').css('display', 'none');
        $('.g2fa_secret', $('#auth-popup')).addClass('show');
        $('.loader', $('#auth-popup')).removeClass('show-loaded');
      } else if (request.status == true && request.g2fa == true && request.secret_enter == true) {
        window.location.replace('/lk');
      } else {
        $('.loader', $('#auth-popup')).removeClass('show-loaded');
        checkAddErrorClass($('#login-email'), 'error', true);
        checkAddErrorClass($('#login-passwd'), 'error', true);
      }
    });
  });

  //Remember Password === POP UP
  $('.start-register .btn-popup', $('#auth-popup')).click(function (e) {
    e.preventDefault();

    var parent_section = $('#auth-popup'),
        reg_firstName = $('#reg-firstname', parent_section),
        reg_secondName = $('#reg-secondname', parent_section),
        reg_mail = $('#reg-email', parent_section),
        reg_mail_repeat = $('#reg-email-repeat', parent_section),
        reg_passwd1 = $('#reg-passwd1', parent_section),
        reg_passwd2 = $('#reg-passwd2', parent_section),
        errorReg = false;

    validFiledRules.nameENRU($(reg_firstName).val(), $(reg_firstName)) ? errorReg = true : '';
    validFiledRules.nameENRU($(reg_secondName).val(), $(reg_secondName)) ? errorReg = true : '';
    validFiledRules.email($(reg_mail).val(), $(reg_mail)) ? errorReg = true : '';
    validFiledRules.email($(reg_mail_repeat).val(), $(reg_mail_repeat)) ? errorReg = true : '';
    validFiledRules.password($(reg_passwd1).val(), $(reg_passwd1)) ? errorReg = true : '';
    validFiledRules.password($(reg_passwd2).val(), $(reg_passwd2)) ? errorReg = true : '';

    validFiledRules.comparePassword($(reg_mail).val(), $(reg_mail_repeat).val(), $(reg_mail), $(reg_mail_repeat)) ? errorReg = true : '';
    validFiledRules.comparePassword($(reg_passwd1).val(), $(reg_passwd2).val(), $(reg_passwd1), $(reg_passwd2)) ? errorReg = true : '';

    console.log(errorReg);

    if (errorReg) {
      return false;
    }

    $('.loader', $('#auth-popup')).addClass('show-loaded');

    $.post("/request", {
      action: "registration",
      email: $(reg_mail).val(),
      password1: $(reg_passwd1).val(),
      password2: $(reg_passwd2).val(),
      g2fa: $('#g2fa').prop('checked')
    }, function (data) {
      var request = JSON.parse(data);
      if (request.status == true && request.qr_code == "") {
        location.reload();
      } else if (request.status == true && request.qr_code != "") {
        $('div.state.register form', $('#auth-popup')).before('<img class="qr" src="' + request.qr_code + '"></img>');
        $('.loader', $('#auth-popup')).removeClass('show-loaded');
      } else {
        $('.loader', $('#auth-popup')).removeClass('show-loaded');
      }
    });
  });
};

var selectMediaAboutUs = function selectMediaAboutUs() {
  $('.item', $('#media-about-us')).click(function () {
    var $name = $(this).data('name'),
        $descr = $(this).data('descr'),
        $who = $(this).data('who'),
        $parent = $('#media-about-us');

    AnimateElement({
      elem: $('.media', $parent),
      duration: 300,
      animateProps: {
        opacity: 0
      },
      funcComplite: function funcComplite() {
        $('.media .media-name a', $parent).attr('href', 'http://' + $name).text($name);
        $('.media .media-txt p:first-child', $parent).text($descr);
        $('.media .media-txt p:last-child', $parent).text($who);

        AnimateElement({
          elem: $('.media', $parent),
          duration: 500,
          animateProps: {
            opacity: 1,
            height: $(this).height()
          },
          funcStart: function funcStart() {
            console.log($(this).height());
          },
          funcComplite: function funcComplite() {
            $(this).removeAttr('style');
          }
        });
      }
    });
  });
};

// Basic complexity of mining (START)

var initMobileComplexity = function initMobileComplexity(elem) {
  var initMobileComplexity = new _dropdown2.default({ elem: elem });
  initMobileComplexity.init_dropdown();

  // INIT GRAPH
  complexityGraph('init', $('#mobile-complexity-mng').val());
};

var combineComplexityMining = function combineComplexityMining() {
  //desk
  $('#desktop-complexity-mng li button').click(function () {
    if ($(this).hasClass('active') || $(this).parents('.wrapp').hasClass('loading')) {
      return;
    }

    var $this_value = $(this).data('cur');

    $('#desktop-complexity-mng button.active').removeClass('active');
    $(this).addClass('active');

    $('#mobile-complexity-mng').val($this_value).trigger('change');
  });

  //mob
  $('#mobile-complexity-mng').on('change', function (e) {
    var $this_value = $(this).val();
    $('#desktop-complexity-mng button.active').removeClass('active');
    $('#desktop-complexity-mng button').each(function (i, item) {
      if ($(item).data('cur') == $this_value) {
        $(this).addClass('active');
        return;
      }
    });

    complexityGraph('update', $this_value);
  });
};

var complexityGraph = function complexityGraph(state, cur) {
  var data = [[new Date("2018/02/13").getTime(), 2874674234416], [new Date("2018/02/14").getTime(), 2874674234416], [new Date("2018/02/15").getTime(), 2874674234416], [new Date("2018/02/16").getTime(), 2874674234416], [new Date("2018/02/17").getTime(), 2874674234416], [new Date("2018/02/18").getTime(), 2874674234416], [new Date("2018/02/19").getTime(), 2874674234416], [new Date("2018/02/20").getTime(), 2967853337745], [new Date("2018/02/21").getTime(), 3007383866430], [new Date("2018/02/22").getTime(), 3007383866430], [new Date("2018/02/23").getTime(), 3007383866430], [new Date("2018/02/24").getTime(), 3007383866430], [new Date("2018/02/25").getTime(), 3007383866430], [new Date("2018/02/26").getTime(), 3007383866430], [new Date("2018/02/27").getTime(), 3007383866430], [new Date("2018/02/28").getTime(), 3007383866430], [new Date("2018/03/01").getTime(), 3007383866430], [new Date("2018/03/02").getTime(), 3007383866430], [new Date("2018/03/03").getTime(), 3007383866430], [new Date("2018/03/04").getTime(), 3007383866430], [new Date("2018/03/05").getTime(), 3233581400367], [new Date("2018/03/06").getTime(), 3290605988755], [new Date("2018/03/07").getTime(), 3290605988755], [new Date("2018/03/08").getTime(), 3290605988755], [new Date("2018/03/09").getTime(), 3290605988755], [new Date("2018/03/10").getTime(), 3290605988755], [new Date("2018/03/11").getTime(), 3290605988755], [new Date("2018/03/12").getTime(), 3290605988755], [new Date("2018/03/13").getTime(), 3290605988755], [new Date("2018/03/14").getTime(), 3290605988755], [new Date("2018/03/15").getTime(), 3290605988755], [new Date("2018/03/16").getTime(), 3290605988755], [new Date("2018/03/17").getTime(), 3290605988755], [new Date("2018/03/18").getTime(), 3381698122496], [new Date("2018/03/19").getTime(), 3462542391192], [new Date("2018/03/20").getTime(), 3462542391192], [new Date("2018/03/21").getTime(), 3462542391192], [new Date("2018/03/22").getTime(), 3462542391192], [new Date("2018/03/23").getTime(), 3462542391192], [new Date("2018/03/24").getTime(), 3462542391192], [new Date("2018/03/25").getTime(), 3462542391192], [new Date("2018/03/26").getTime(), 3462542391192], [new Date("2018/03/27").getTime(), 3462542391192], [new Date("2018/03/28").getTime(), 3462542391192], [new Date("2018/03/29").getTime(), 3462542391192], [new Date("2018/03/30").getTime(), 3462542391192], [new Date("2018/03/31").getTime(), 3462542391192], [new Date("2018/04/01").getTime(), 3494288842680], [new Date("2018/04/02").getTime(), 3511060552900], [new Date("2018/04/03").getTime(), 3511060552900], [new Date("2018/04/04").getTime(), 3511060552900], [new Date("2018/04/05").getTime(), 3511060552900], [new Date("2018/04/06").getTime(), 3511060552900], [new Date("2018/04/07").getTime(), 3511060552900], [new Date("2018/04/08").getTime(), 3511060552900], [new Date("2018/04/09").getTime(), 3511060552900], [new Date("2018/04/10").getTime(), 3511060552900], [new Date("2018/04/11").getTime(), 3511060552900], [new Date("2018/04/12").getTime(), 3511060552900], [new Date("2018/04/13").getTime(), 3511060552900], [new Date("2018/04/14").getTime(), 3796188328005], [new Date("2018/04/15").getTime(), 3839316899030], [new Date("2018/04/16").getTime(), 3839316899030], [new Date("2018/04/17").getTime(), 3839316899030], [new Date("2018/04/18").getTime(), 3839316899030], [new Date("2018/04/19").getTime(), 3839316899030], [new Date("2018/04/20").getTime(), 3839316899030], [new Date("2018/04/21").getTime(), 3839316899030], [new Date("2018/04/22").getTime(), 3839316899030], [new Date("2018/04/23").getTime(), 3839316899030], [new Date("2018/04/24").getTime(), 3839316899030], [new Date("2018/04/25").getTime(), 3839316899030], [new Date("2018/04/26").getTime(), 3839316899030], [new Date("2018/04/27").getTime(), 3943557223452], [new Date("2018/04/28").getTime(), 4022059196165], [new Date("2018/04/29").getTime(), 4022059196165], [new Date("2018/04/30").getTime(), 4022059196165], [new Date("2018/05/01").getTime(), 4022059196165], [new Date("2018/05/02").getTime(), 4022059196165], [new Date("2018/05/03").getTime(), 4022059196165], [new Date("2018/05/04").getTime(), 4022059196165], [new Date("2018/05/05").getTime(), 4022059196165], [new Date("2018/05/06").getTime(), 4022059196165], [new Date("2018/05/07").getTime(), 4022059196165], [new Date("2018/05/08").getTime(), 4022059196165], [new Date("2018/05/09").getTime(), 4022059196165], [new Date("2018/05/10").getTime(), 4022059196165], [new Date("2018/05/11").getTime(), 4138123233246], [new Date("2018/05/12").getTime(), 4143878474754], [new Date("2018/05/13").getTime(), 4143878474754], [new Date("2018/05/14").getTime(), 4143878474754], [new Date("2018/05/15").getTime(), 4143878474754], [new Date("2018/05/16").getTime(), 4143878474754], [new Date("2018/05/17").getTime(), 4143878474754], [new Date("2018/05/18").getTime(), 4143878474754], [new Date("2018/05/19").getTime(), 4143878474754], [new Date("2018/05/20").getTime(), 4143878474754], [new Date("2018/05/21").getTime(), 4143878474754], [new Date("2018/05/22").getTime(), 4143878474754], [new Date("2018/05/23").getTime(), 4143878474754], [new Date("2018/05/24").getTime(), 4219142059013], [new Date("2018/05/25").getTime(), 4306949573982], [new Date("2018/05/26").getTime(), 4306949573982], [new Date("2018/05/27").getTime(), 4306949573982], [new Date("2018/05/28").getTime(), 4306949573982], [new Date("2018/05/29").getTime(), 4306949573982], [new Date("2018/05/30").getTime(), 4306949573982], [new Date("2018/05/31").getTime(), 4306949573982], [new Date("2018/06/01").getTime(), 4306949573982], [new Date("2018/06/02").getTime(), 4306949573982], [new Date("2018/06/03").getTime(), 4306949573982]];
  var data2TEST = [[new Date("2018/02/13").getTime(), 2874674234416], [new Date("2018/02/14").getTime(), 2874674234416], [new Date("2018/02/15").getTime(), 2874674234416], [new Date("2018/02/16").getTime(), 2874674234416]];
  var complexityGrap = new _cabinet__generateMyData.cabinet_generateMyData('graph-complexity-of-mining');

  if (state == "init") {
    complexityGrap.init_area_GraphJSON_complexity(cur);
  } else {
    $('.complexity-of-mining .wrapp', $('#personal_cabinet')).addClass('loading');
    complexityGrap.showLoad();
    complexityGrap.update_date(cur);
  }
};
// Basic complexity of mining (END)

var generateTOP100 = function generateTOP100() {
  var TABLE = $('#top-100'),
      parent_block = $('#personal_cabinet'),
      cntCur = $(TABLE).find('tbody > tr'),
      nameColArr = [],
      nameColumn = $(TABLE).find('thead > tr > td').each(function (i, item) {
    return nameColArr.push($(item).html());
  });

  var _loop = function _loop() {
    $('.wrapp-top-100', parent_block).append('<div class="item"><div class="data"></div><div class="footer"></div</div>');

    var dataTable = $(cntCur).eq(i);

    nameColArr.map(function (item, i) {
      $('.wrapp-top-100 .item .data', parent_block).last().append('<div><p class="title-top-100">' + item + '</p><p class="' + $(dataTable).find('td').eq(i).attr('class') + '">' + $(dataTable).find('td').eq(i).html() + '</p></div>');
    });

    $('.wrapp-top-100 .item .footer', parent_block).last().append('<div class="desc"><p>' + $(dataTable).find('td').eq(0).html() + '</p></div>');
  };

  for (var i = 0; i < cntCur.length; i++) {
    _loop();
  }

  topCryptocurrencies();
};

var PCwalletWork = function PCwalletWork() {

  //edit
  $('.edit-wallet', $('#personal_cabinet')).click(function () {

    if ($(this).parents('.item').hasClass('edit')) {
      $(this).parents('.item').removeClass('edit');
    } else {
      $(this).parents('.item').addClass('edit');
    }
  });

  //Apply edit Wallet
  $('.edit-wallet-btn .btn-long', $('#personal_cabinet')).click(function (e) {
    e.preventDefault();
    var $this_new_value = $(this).parents('.item').find('input'),
        errorEnter = false;

    validFiledRules.walletNumber($($this_new_value).val(), $($this_new_value)) ? errorEnter = true : '';

    if (errorEnter) {
      return false;
    }

    $($this_new_value).parents('div.base-right').children('.loader').addClass('show-loaded');
    $.post("https://reqres.in/api/users?delay=3", { action: 'subscribe', numberWallet: $($this_new_value).val() }).done(function (data) {
      // let request = JSON.parse(data);
      console.log(data);
      $($this_new_value).parents('div.base-right').children('.loader').removeClass('show-loaded');
      $($this_new_value).parents('div.item').removeClass('edit');
    }).fail(function () {
      $($this_new_value).parents('div.base-right').children('.loader').removeClass('show-loaded');
    });
  });

  //Add new wallet
  $('.add-wallet-btn', $('#personal_cabinet')).click(function (e) {
    e.preventDefault();
    var $this_new_value = $(this).parents('.wrapp-input').find('input'),
        errorEnter = false;

    validFiledRules.walletNumber($($this_new_value).val(), $($this_new_value)) ? errorEnter = true : '';

    if (errorEnter) {
      return false;
    }

    $($this_new_value).parents('div.base-right').children('.loader').addClass('show-loaded');
    $.post("https://reqres.in/api/users?delay=3", { action: 'subscribe', numberWallet: $($this_new_value).val() }).done(function (data) {
      // let request = JSON.parse(data);
      location.reload();
    }).fail(function () {
      $($this_new_value).parents('div.base-right').children('.loader').removeClass('show-loaded');
    });
  });
};

function personalCabinetMoveBlock(state) {
  if (state) {
    $('div.graph-and-data .base-left', $('#personal_cabinet')).prepend($('.graph-and-data .auth-info'));
    $('.graph-and-data .my-mining-data.data').insertAfter($('div.graph-and-data .base-left .my-mining-data.graph', $('#personal_cabinet')));
  } else {
    $('div.graph-and-data .base-right', $('#personal_cabinet')).prepend($('.graph-and-data .my-mining-data.data'));
    $('div.graph-and-data .base-right', $('#personal_cabinet')).prepend($('.graph-and-data .auth-info'));
  }
}

function editPersonalCabinet() {
  var $PARENT_ELEMENT = $('#personal_cabinet');

  //OPEN EDIT PROFILE(START)
  $('.auth-info-edit-profile', $PARENT_ELEMENT).click(function (e) {
    e.preventDefault();

    if ($('.graph-and-data .base-left').hasClass('settings-style')) {
      $('.graph-and-data .base-left').removeClass('settings-style');
      $(this).text('Edit profile data');
    } else {
      $('.graph-and-data .base-left').addClass('settings-style');
      $(this).text('Exit from profile editing');
    }
  });
  //OPEN EDIT PROFILE(END)

  //PERSORAL DATA (START)
  //Unlock input
  $('.settings-user .edit-field', $PARENT_ELEMENT).click(function () {
    if ($(this).hasClass('edit')) {
      $(this).removeClass('edit').addClass('cancel');
      $(this).siblings('input').removeAttr('readonly').focus();
    } else {
      $(this).removeClass('cancel').addClass('edit');
      $(this).siblings('input').prop('readonly', 'true');
      $(this).siblings('input').removeClass('error');
      $(this).siblings('input').val($(this).siblings('input').attr('value'));
    }
  });

  //Save CHANGES
  $('.settings-user--personal-data .save-changes .btn-long', $PARENT_ELEMENT).click(function (e) {
    e.preventDefault();

    var $this = $(this),
        fname_fld = $('#cabinet_fname'),
        lname_fld = $('#cabinet_lname'),
        email_fld = $('#cabinet_email'),
        errorChange = false;

    validFiledRules.nameENRU($(fname_fld).val(), $(fname_fld)) ? errorChange = true : '';
    validFiledRules.nameENRU($(lname_fld).val(), $(lname_fld)) ? errorChange = true : '';
    validFiledRules.email($(email_fld).val(), $(email_fld)) ? errorChange = true : '';

    if (errorChange) {
      return false;
    }

    $(this).parents('div.wrapp').find('.loader').addClass('show-loaded');

    $.post("https://reqres.in/api/users", { fname: $('#cabinet_fname').val(), lname: $('#cabinet_lname').val(), email: $('#cabinet_email').val() }).done(function (data) {
      console.log(data);
      // let request = JSON.parse(data);
      // if ( request.status == true ) {
      $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
      // } else {
      $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
      // }
    }).fail(
    // DO SOMETHING
    function () {
      $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
    });
  });
  //PERSORAL DATA (END)

  //CHANGE PASSWORD (START)
  $('.settings-user--auth-data .save-changes .btn-long', $PARENT_ELEMENT).click(function (e) {
    e.preventDefault();

    var $this = $(this),
        old_passwd = $('#cabinet_oldpasswd'),
        new_passwd1 = $('#cabinet_newpassword'),
        new_passwd2 = $('#cabinet_newpassword_again'),
        errorChangePass = false;

    validFiledRules.password($(old_passwd).val(), $(old_passwd)) ? errorChangePass = true : '';
    validFiledRules.password($(new_passwd1).val(), $(new_passwd1)) ? errorChangePass = true : '';
    validFiledRules.password($(new_passwd2).val(), $(new_passwd2)) ? errorChangePass = true : '';

    validFiledRules.comparePassword($(new_passwd1).val(), $(new_passwd2).val(), $(new_passwd1), $(new_passwd2)) ? errorChangePass = true : '';

    if (errorChangePass) {
      return false;
    }

    $(this).parents('div.wrapp').find('.loader').addClass('show-loaded');

    $.post("https://reqres.in/api/users", { old_passwd: $(old_passwd).val(), new_passwd1: $(new_passwd1).val(), new_passwd2: $(new_passwd2).val() }).done(function (data) {
      // let request = JSON.parse(data);
      // if ( request.status == true ) {
      $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
      // } else {
      $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
      // }
    }).fail(
    // DO SOMETHING
    function () {
      $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
    });
  });
  //CHANGE PASSWORD (END)

  //CHECK GOOGLE AUTH (START)
  $('.security-setting--selector .btn-long', $PARENT_ELEMENT).click(function (e) {
    e.preventDefault();

    var $this = $(this);

    $(this).parents('div.wrapp').find('.loader').addClass('show-loaded');

    function sendNewStateGoogleAuth(state) {
      $.post("https://reqres.in/api/users", { googleauth: $('#google-auth').prop('checked') }).done(function (data) {
        console.log(data.googleauth);
        // let request = JSON.parse(data);
        // if ( request.status == true ) {
        if (data.googleauth == 'true') {
          $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
          $('#google-auth').prop('checked', false);
          $($this).children('span').text('Disabled');
        } else {
          console.log('here');
          $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
          $('#google-auth').prop('checked', true);
          $($this).children('span').text('Enabled');
        }
      }).fail(
      // DO SOMETHING
      function () {
        $($this).parents('div.wrapp').find('.loader').removeClass('show-loaded');
      });
    }

    sendNewStateGoogleAuth($('#google-auth').prop('checked'));
  });
  //CHECK GOOGLE AUTH (END)
}

function checkMiningState() {
  //Init load some data to table id="kind_of_type_mining"
  $('#summary-data tbody tr').click(function () {
    var $this = this;

    $($this).parents('div.base-table-style-wrapp').children('loader').addClass('show-loaded');

    $.get("/request", { action: 'getAnalystData', id: $(this).data('id') }).done(function (data) {
      //REMOVE ALL OLD TD
      $("#kind_of_type_mining tbody tr").remove();

      //WORK WITH ARRAY
      data.map(function (item, i) {
        $('#kind_of_type_mining tbody').append('<tr></tr>');

        for (var x = 0; x < item.length; x++) {
          if (x == 0) {
            $('#kind_of_type_mining tbody tr').last().append('<td><span class="circle circle-' + item[x] + '"></span></td>');
          } else {
            $('#kind_of_type_mining tbody tr').last().append('<td>' + item[x] + '</td>');
          }
        }
      });

      //REINIT SORT
      $('#kind_of_type_mining').trigger('update');
      $($this).parents('div.base-table-style-wrapp').children('loader').removeClass('show-loaded');
    }).fail(function () {
      $($this).parents('div.base-table-style-wrapp').children('loader').removeClass('show-loaded');
    });
  });

  initSortTable();
}

function initSortTable() {
  $("#kind_of_type_mining").tablesorter({ sortList: [[1, 0]] });
}

function scrollToChooseMiningPlan() {
  $('#cloud-mining .par-now a.btn-long').click(function (e) {
    e.preventDefault();

    $('html, body').animate({
      scrollTop: $('.mining-plan', $('#cloud-mining')).offset().top
    });
  });
}

// ====== ASIC (START)
if (document.body.classList.contains('asic')) {
  //DATE RANGE CLUSTER (START)
  $('input[name="daterange-asic-cluster"]').daterangepicker({
    autoUpdateInput: false,
    opens: 'left',
    applyButtonClasses: 'calendar--btn-asic calendar--btn-apply',
    cancelButtonClasses: 'calendar--btn-asic calendar--btn-cancel',
    "locale": {
      "format": "DD/MM/YYYY"
    }
  }, function (start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });

  $('input[name="daterange-asic-cluster"]').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });
  //DATE RANGE CLUSTER (END)

  $('#choose-mining').select2({
    minimumResultsForSearch: Infinity, // disabled search
    containerCssClass: 'dd___base-container',
    dropdownCssClass: 'dd___base-dropdown',
    dropdownParent: $('#choose-mining').parent('div'),
    width: "100%",
    placeholder: $('#choose-mining').data('placeholder')
  });

  //CLUSTER MINING (START)

  //CLUSTER MINING (END)
}

// ====== ASIC (END)

/***/ })

/******/ });