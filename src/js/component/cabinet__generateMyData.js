class cabinet_generateMyData {

  constructor(id, count) {
    this.id        = id;
    this.count     = count;
  }

  init_area_GraphJSON() {
      var $thisID     = this.id;
      var $thisCount  = this.count;

      $.getJSON(
          '/request',
          {action: 'getGraph', key: this.id, count: $thisCount},
          function (data) {
              Highcharts.chart($thisID, {
                  chart: {
                      zoomType: 'x'
                  },
                  title: {
                      text: ''
                  },
                  // subtitle: {
                  //     text: document.ontouchstart === undefined ?
                  //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                  // },
                  xAxis: {
                      type: 'datetime'
                  },
                  yAxis: {
                      min: 0,
                      title: {
                          text: ''
                      }
                  },
                  legend: {
                      enabled: false
                  },
                  plotOptions: {
                      area: {
                        fillColor: {
                              linearGradient: {
                                  x1: 0,
                                  y1: 0,
                                  x2: 0,
                                  y2: 1
                              },
                              stops: [
                                  [0, '#c8c2f3'],
                                  [1, 'rgba(255, 255, 255, 0.92)']
                              ]
                        },
                        marker: {
                          radius: 2,
                        },
                        lineColor: '#4791cf',
                        lineWidth: 2,
                        states: {
                            // hover: {
                            //     lineWidth: 1
                            // }
                        },
                        threshold: null
                      },
                  },
                  series: [{
                      type: 'area',
                      name: $('#' + $thisID).data('lng__name-grap'),
                      data: data.data
                  }]
              });
          }
      );
  }

  init_area_GraphJSON_complexity(cur) {
      var $thisID     = this.id;
      var $thisCount  = this.count;
      var $thisCur    = cur;

      $.getJSON(
          '/request',
          {action: 'getGraph', key: this.id, count: $thisCount},
          function (data) {
              let chart = $('#' + $thisID).highcharts();
              let arrayGraph = [];

              data.data.map(function( item, i ){
                if ( item.curName == $thisCur ) {
                  arrayGraph = item.graphData
                }
              })
              Highcharts.chart($thisID, {
                  chart: {
                      zoomType: 'x'
                  },
                  title: {
                      text: ''
                  },
                  // subtitle: {
                  //     text: document.ontouchstart === undefined ?
                  //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                  // },
                  xAxis: {
                      type: 'datetime'
                  },
                  yAxis: {
                      min: 0,
                      title: {
                          text: ''
                      }
                  },
                  legend: {
                      enabled: false
                  },
                  plotOptions: {
                      area: {
                        fillColor: {
                              linearGradient: {
                                  x1: 0,
                                  y1: 0,
                                  x2: 0,
                                  y2: 1
                              },
                              stops: [
                                  [0, '#c8c2f3'],
                                  [1, 'rgba(255, 255, 255, 0.92)']
                              ]
                        },
                        marker: {
                          radius: 2,
                        },
                        lineColor: '#4791cf',
                        lineWidth: 2,
                        states: {
                            // hover: {
                            //     lineWidth: 1
                            // }
                        },
                        threshold: null
                      },
                  },
                  series: [{
                      type: 'area',
                      name: $('#' + $thisID).data('lng__name-grap'),
                      data: arrayGraph
                  }]
              });
          }
      );
  }

  init_area_GraphJSON_cluster() {
      var $thisID     = this.id;
      var $thisCount  = this.count;

      $.getJSON(
          '/request',
          {action: 'getGraph', key: this.id, count: $thisCount},
          function (data) {
              Highcharts.chart($thisID, {
                  chart: {
                      zoomType: 'x'
                  },
                  title: {
                      text: ''
                  },
                  // subtitle: {
                  //     text: document.ontouchstart === undefined ?
                  //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                  // },
                  xAxis: {
                      type: 'datetime'
                  },
                  yAxis: {
                      min: 0,
                      title: {
                          text: ''
                      }
                  },
                  legend: {
                      enabled: false
                  },
                  plotOptions: {
                      area: {
                        fillColor: {
                              linearGradient: {
                                  x1: 0,
                                  y1: 0,
                                  x2: 0,
                                  y2: 1
                              },
                              stops: [
                                  [0, '#c8c2f3'],
                                  [1, 'rgba(255, 255, 255, 0.92)']
                              ]
                        },
                        marker: {
                          radius: 2,
                        },
                        lineColor: '#4791cf',
                        lineWidth: 2,
                        states: {
                            // hover: {
                            //     lineWidth: 1
                            // }
                        },
                        threshold: null
                      },
                  },
                  series: [{
                      type: 'area',
                      name: $('#' + $thisID).data('lng__name-grap'),
                      data: data.data
                  }]
              });
          }
      ); 
  }

  showLoad() {
    var chart = $('#' + this.id).highcharts();
    console.log( chart );
    chart.showLoading('Loading...');
  }

  update_date(cur) {
      var $thisID     = this.id;
      var $thisCount  = this.count;
      var $thisCur    = cur;

      $.getJSON(
          '/request',
          {action: 'getGraph', key: this.id, count: $thisCount},
          function (data) {
            var chart = $('#' + $thisID).highcharts();
            var arrayGraph = [];

            if ( $thisCur != 'all' ) {

              while(chart.series.length > 0) {
                chart.series[0].remove(true);
              }

              data.data.map(function (item, i) {
                if (item.curName == $thisCur) {
                    arrayGraph = item.graphData;

                  chart.addSeries({
                    name: item.curName,
                    data: item.graphData
                  });
                }
              });


            } else {
              chart.series[0].remove( false );

              data.data.map(function (item, i) {
                chart.addSeries({
                  name: item.curName,
                  data: item.graphData
                });
              });

              chart.redraw();
            }

            $('.complexity-of-mining .wrapp', $('#personal_cabinet')).removeClass('loading');
            chart.hideLoading();
          }
      );
  }

  change_show_point(new_count) {
      var $thisID     = this.id;
      var $new_count  = new_count;

      $.getJSON(
          '/request',
          {action: 'getGraph', key: this.id, count: new_count},
          function (data) {
              Highcharts.chart($thisID, {
                  chart: {
                      zoomType: 'x'
                  },
                  title: {
                      text: ''
                  },
                  // subtitle: {
                  //     text: document.ontouchstart === undefined ?
                  //             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                  // },
                  xAxis: {
                      type: 'datetime'
                  },
                  yAxis: {
                      min: 0,
                      title: {
                          text: ''
                      }
                  },
                  legend: {
                      enabled: false
                  },
                  plotOptions: {
                      area: {
                        fillColor: {
                              linearGradient: {
                                  x1: 0,
                                  y1: 0,
                                  x2: 0,
                                  y2: 1
                              },
                              stops: [
                                  [0, '#c8c2f3'],
                                  [1, 'rgba(255, 255, 255, 0.92)']
                              ]
                        },
                        marker: {
                          radius: 2,
                        },
                        lineColor: '#4791cf',
                        lineWidth: 2,
                        states: {
                            // hover: {
                            //     lineWidth: 1
                            // }
                        },
                        threshold: null
                      },
                  },
                  series: [{
                      type: 'area',
                      name: $('#' + $thisID).data('lng__name-grap'),
                      data: data.data
                  }]
              });
          }
      );    
  }
}

export { cabinet_generateMyData }