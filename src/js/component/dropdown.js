class dropdown {

  constructor(obj) {
    this.elem      = obj.elem;
  }

  init_dropdown() {
    $(this.elem).select2({
      minimumResultsForSearch: Infinity, // disabled search
      containerCssClass: 'dd___base-container',
      dropdownCssClass: 'dd___base-dropdown',
      dropdownParent: $(this.elem).parent('div'),
      width: "100%",
    });
  }

  // triggerChange_dropdown() {
  //   $(this.elem).on('select2:open', function(e) {
  //     console.log( 'hi' );
  //   });
  // }
}

export default dropdown